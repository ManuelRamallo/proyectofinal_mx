var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const config = require('./config');
const auth = require('./middlewares/auth');
const cors = require('cors');
const moment = require('moment');
const fileUpload = require('express-fileupload');

mongoose.Promise = global.Promise;
mongoose.connect(config.MONGODB_URI, {useMongoClient: true})
    .then(
        () => {

            const User = require('./models/users');

            User.find((err, usuario) => {
                if(!(usuario && usuario.length)){
                    let user1 = new User({
                       nombre: "Manuel",
                       apellidos: "Ramallo Díaz",
                       email: "mramallodiaz96@gmail.com",
                       password: "1234"
                    });

                    user1.save((err, usuario) =>{
                        if (err) return console.error(`Error de insercion: ${err.message}`);
                        console.log(`Insertado: ${usuario}`);
                    });

                } else {
                    console.log('Ya hay datos de usuario');
                }
            });

            const Carreras = require('./models/carreras');

            Carreras.find((err, carreras) =>{
                if(!(carreras && carreras.length)){

                    let carrera1 = new Carreras({
                        nombre_circuito: "RedSand Mx Park",
                        nombre_lugar: "MXGP de La comunidad Valenciana",
                        direccion: "CV15 Km 3,5 Vilafames (Castellon)",
                        fecha: "23/03/2017",
                        web: "http://www.redsandmxpark.es",
                        telefono: "+34 630226711",
                        pais: "http://flags.fmcdn.net/data/flags/w580/es.png",
                        foto_carrera: "http://www.mxgp.com/sites/default/files/track/image/DJI_0002.00_35_35_32.Imagen%20fija064.jpg",
                        url_entrada: "https://www.eventbrite.com/e/mxgp-de-la-comunidad-valenciana-tickets-44371997840"
                    });

                    carrera1.save((err, carreras) => {
                        if(err) return console.error(`Error de insercion: ${err.message}`);
                        console.log(`Insertado: ${carreras}`);
                    });
                } else {
                    console.log('Ya hay datos de carreras')
                }
            });

            const Pilotos = require('./models/pilotos');

            Pilotos.find((err, pilotos) => {

                if(!(pilotos && pilotos.length)){

                    let piloto1 = new Pilotos({
                        nombre_completo: "Jeffrey Herlings",
                        equipo: "Red Bull KTM Factory Racing",
                        edad: "12/09/1994",
                        nacionalidad: "https://upload.wikimedia.org/wikipedia/commons/2/20/Flag_of_the_Netherlands.svg",
                        dorsal: "84",
                        titulos_mundiales: "3",
                        titulos_gp: "68",
                        primer_gp: "2010 Grand Prix de Holanda, Valkenswaard, MX2",
                        foto_piloto_perfil: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjMIITBsHk24RivPlg_uVnk9645iLd_LFqgdgBzjgcH3qb6WUURg",
                        foto_piloto: "http://gatedrop.com/wp-content/uploads/2017/09/164892_Jeffrey-Herlings-KTM-450-SX-F-2017.jpg",
                        posicion_campeonato_anterior: "2º"
                    });

                    piloto1.save((err, pilotos) => {
                        if(err) return console.error(`Error de insercion: ${err.message}`);
                        console.log(`Insertado: ${pilotos}`);
                    });
                } else {
                    console.log('Ya hay datos de pilotos');
                }


            });

        },(err) => {
            console.error(`Error de conexión: ${err.message}`);
        }
    );


const users = require('./routes/users');
const carreras = require('./routes/carreras');
const pilotos = require('./routes/pilotos');

let app = express();

app.use(logger('dev'));
app.use(cors());
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/v1/auth', users);
app.use('/api/v1/carreras', auth.isAuth, carreras);
app.use('/api/v1/pilotos', auth.isAuth, pilotos);

module.exports = app;

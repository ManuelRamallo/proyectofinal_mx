const Carreras = require('../models/carreras');
const User = require('../models/users');

const moment = require('moment');


//GET Obtener todas las carreras /list
module.exports.findAllCarreras = (req, res) => {

    Carreras.find((err, carreras) => {
       if(err) return res.status(404).jsonp({
           error: 404,
           mensaje: `${err.message}`
       });

       if (carreras && carreras.length){
           res.status(200).jsonp(carreras);
       } else {
           res.sendStatus(404)
       }
    });
};

//GET Mostrar detalles de una carrera
module.exports.findOneCarrera = (req, res) => {

    Carreras.findById(req.params.id, (err, carrera) => {
        if(err) return res.status(404).jsonp({error: 404, mensaje: 'No existe una carrera con ese Id'});

        res.status(200).jsonp(carrera)
    });
};

//POST Añadir carrera
module.exports.addCarrera = (req, res) => {

    User.findOne({_id: req.user}, (err, user) => {
        if (err) return res.status(401).jsonp({error: 401, mensaje: 'Error en la autenticación'});
        if (!user) return res.status(404).jsonp({error: 404, mensaje: 'El usuario no existe'});

        let nuevaCarrera = new Carreras ({
            nombre_circuito: req.body.nombre_circuito,
            nombre_lugar: req.body.nombre_lugar,
            direccion: req.body.direccion,
            fecha: req.body.fecha,
            web: req.body.web,
            telefono: req.body.telefono,
            pais: req.body.pais,
            foto_carrera: req.body.foto_carrera,
            url_entrada: req.body.url_entrada
        });

        nuevaCarrera.save(function (err, carrera) {
            if(err) return res.status(404).jsonp({error: 404, mensaje: `${err.message}`});
            res.status(201).jsonp(carrera);
        });
    });
};

//DELETE Eliminar una carrera
module.exports.deleteCarrera = (req, res) => {

    Carreras.findById(req.params.id, (err, carrera) => {
        if(carrera === undefined)
            return res.sendStatus(404);

        carrera.remove((err) => {
            if(err) return res.status(404).jsonp({error: 404, mensaje: `${err.message}`});
            res.sendStatus(204);
        });
    });
};


//PUT Editar una carrera
module.exports.editCarrera = (req, res) => {

    Carreras.findById(req.params.id, (err,carrera) => {
       if(err) return res.status(401).jsonp({error: 401, mensaje: `Error al buscar por ID`});

       if(!carrera) return res.status(404).jsonp({error: 404, mensaje: `Carrera no encontrada`});

       if(req.body.nombre_circuito)
           carrera.nombre_circuito = req.body.nombre_circuito;
       if(req.body.nombre_lugar)
           carrera.nombre_lugar = req.body.nombre_lugar;
       if(req.body.direccion)
           carrera.direccion = req.body.direccion;
       if(req.body.fecha)
           carrera.fecha = req.body.fecha;
       if(req.body.web)
           carrera.web = req.body.web;
       if(req.body.telefono)
           carrera.telefono = req.body.telefono;
       if(req.body.pais)
           carrera.pais = req.body.pais;
       if(req.body.foto_carrera)
           carrera.foto_carrera = req.body.foto_carrera;
       if(req.body.url_entrada)
           carrera.url_entrada = req.body.url_entrada;

       carrera.save(function (err, result){
          if(err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});

          res.status(200).jsonp({
             nombre_circuito: result.nombre_circuito,
             nombre_lugar: result.nombre_lugar,
             direccion: result.direccion,
             fecha: result.fecha,
             web: result.web,
             telefono: result.telefono,
             pais: result.pais,
             foto_carrera: result.foto_carrera,
             url_entrada: result.url_entrada
          });
       });

    });
};

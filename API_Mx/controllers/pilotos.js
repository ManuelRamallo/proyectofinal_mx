const Pilotos = require('../models/pilotos');
const User = require('../models/users');

const moment = require('moment');

//GET Obtener todos los pilotos /list
module.exports.findAllPilotos = (req, res) => {

    Pilotos.find((err, pilotos) => {
        if(err) return res.status(404).jsonp({error: 404, mensaje: `${err.message}`});

        if(pilotos && pilotos.length){
            res.status(200).jsonp(pilotos);
        } else {
            res.sendStatus(404)
        }
    });
};

//GET Mostrar detalles de una carrera
module.exports.findOnePiloto = (req, res) => {

    Pilotos.findById(req.params.id, (err, piloto) => {
       if(err) return res.status(404).jsonp({error: 404, mensaje: `No existen pilotos`});

       res.status(202).jsonp(piloto);
    });

};
const bcrypt = require('bcrypt-nodejs');
const service = require('../services');
const User = require('../models/users');
const imgur = require('imgur')


//POST para crear un nuevo usuario(Registrarse)
module.exports.signUp = (req, res) => {
    if(!req.files){
        addUsuario(req, res, null);
    } else {
        let photo = req.files.photo;
        let imagen = photo.data.toString('base64');

        imgur.setClientId('eb456c854ca5051');

        imgur.uploadBase64(imagen)
            .then(function (json) {
                addUsuario(req, res, json.data.link);
            })
            .catch(function (err) {
                addUsuario(req, res, null);
            })
    }
};

function addUsuario(req, res, img) {
    let user = new User({
        nombre: req.body.nombre,
        apellidos: req.body.apellidos,
        email: req.body.email,
        photo: img,
        password: req.body.password
    });

    user.save((err, result) => {
        if(err) return res.status(400).jsonp({
            error: 400,
            mensaje: `${err.message}`
        });

        return res.status(201).jsonp({
            token: service.createToken(user),
            nombre: result.nombre,
            apellidos: result.apellidos,
            email: result.email,
            photo: result.photo
        });
    });
}

//POST Login
module.exports.signIn = (req, res) => {

    User.findOne({email: req.body.email}).select('_id email +password nombre apellidos').exec((err, user) => {

        if (err) return res.status(401).jsonp({error: 401, mensaje: 'Error en la autenticación'});
        if (!user) return res.status(404).jsonp({error: 404, mensaje: 'No existe el usuario'});

        bcrypt.compare(req.body.password, user.password, (err, result) => {

            if (err) return res.status(401).jsonp({error: 401, mensaje: 'Error en la autenticación'});
            if (result == false) return res.status(401).jsonp({error: 401, mensaje: 'Error en la autenticación'});

            else {
                req.user = user;
                res.status(200).jsonp({
                   mensaje: 'Login correcto',
                   token: service.createToken(user),
                   nombre: user.nombre,
                   apellidos: user.apellidos,
                   email: user.email,
                   _id: user._id
                });
            }
        });
    });
};

//GET Listar los usuarios
module.exports.list = (req, res) =>  {

    User.find()
        .select('_id nombre apellidos email').exec((err, result) => {

        if (err) return res.status(500).jsonp({
            error: 500,
            mensaje: err.message
        });

        if (result && result.length) {
            res.status(200).jsonp(result);
        } else {
            res.sendStatus(404);
        }

    });
};

//GET Detalles de un usuario
module.exports.findOneUser = (req, res) => {

    User.findOne({_id: req.user}, (err, usuario) => {
        if(err) return res.status(404).jsonp({error: 404, mensaje: 'No existe un usuario con ese Id'});

        res.status(202).jsonp(usuario);

    });
};

//PUT Editar usuario
module.exports.editUsuario = (req, res) => {
    if(!req.files){
        editUsuario(req, res, null);
    } else {
        let photo = req.files.photo;
        let imagen = photo.data.toString('base64');

        imgur.setClientId('eb456c854ca5051');

        imgur.uploadBase64(imagen)
            .then(function (json) {
                editUsuario(req, res, json.data.link);
            })
            .catch(function (err) {
                editUsuario(req, res, null);
            })
    }

};


function editUsuario(req, res, img){

    User.findOne({_id: req.user}, (err, usuario) => {
        if(err) return res.status(401).jsonp({error: 401, mensaje: `Error de autenticación`});

        if(!usuario) return res.status(404).jsonp({error: 404, mensaje: `Usuario no encontrado`});

        if(req.body.nombre)
            usuario.nombre = req.body.nombre;
        if(req.body.apellidos)
            usuario.apellidos = req.body.apellidos;
        if(req.body.email)
            usuario.email = req.body.email;
        if(req.body.password)
            usuario.password = req.body.password;
        if(img != null)
            usuario.photo = img;

        usuario.save(function (err, result) {
            if(err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});

            res.status(200).jsonp({
               nombre: result.nombre,
               apellidos: result.apellidos,
               email: result.email,
               password: result.password,
               photo: result.photo
            });
        });
    });

};


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let carrerasSchema = new Schema({

   nombre_circuito: String,
   nombre_lugar: String,
   direccion: String,
   fecha: String,
   web: String,
   telefono: String,
   pais: String,
   foto_carrera: String,
   url_entrada: String

});

module.exports = mongoose.model('Carreras', carrerasSchema);
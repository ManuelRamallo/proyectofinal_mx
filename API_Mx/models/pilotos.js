const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let pilotosSchema = new Schema({

    nombre_completo: String,
    equipo: String,
    edad: String,
    nacionalidad: String,
    dorsal: String,
    titulos_mundiales: String,
    titulos_gp: String,
    primer_gp: String,
    foto_piloto_perfil: String,
    foto_piloto: String,
    posicion_campeonato_anterior: String

});

module.exports = mongoose.model('Pilotos', pilotosSchema);
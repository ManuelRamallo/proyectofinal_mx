const express = require('express');
const carrerasControler = require('../controllers/carreras');

let router = express.Router();

router.get('/list', carrerasControler.findAllCarreras);
router.get('/detalles/:id', carrerasControler.findOneCarrera);
router.post('/add', carrerasControler.addCarrera);
router.delete('/delete/:id', carrerasControler.deleteCarrera);
router.put('/edit/:id', carrerasControler.editCarrera);

module.exports = router;
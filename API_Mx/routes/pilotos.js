const express = require('express');
const pilotosControler = require('../controllers/pilotos');

let router = express.Router();

router.get('/list', pilotosControler.findAllPilotos);
router.get('/detalles/:id', pilotosControler.findOnePiloto);


module.exports = router;

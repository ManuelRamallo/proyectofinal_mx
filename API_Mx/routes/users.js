const express = require('express');
const userController = require('../controllers/users');
const auth = require('../middlewares/auth');
let router = express.Router();


router.post('/register', userController.signUp);
router.post('/login', userController.signIn);
router.get('/list', auth.isAuth, userController.list);
router.get('/detalles/:id', auth.isAuth, userController.findOneUser);
router.put('/edit/:id', auth.isAuth, userController.editUsuario);


module.exports = router;

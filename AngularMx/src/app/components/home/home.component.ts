import {Component, Input, OnInit} from '@angular/core';
import {Carrera} from '../../models/Carrera';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {CarrerasService} from '../../services/carreras.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @Input() carrera: Carrera;
  modalRef: NgbModalRef;
  codigo = '';

  constructor(private modalService: NgbModal, private carrerasService: CarrerasService) { }

  ngOnInit() {
  }

}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Carrera} from '../../models/Carrera';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {CarrerasService} from '../../services/carreras.service';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {log} from 'util';

@Component({
  selector: 'app-listado-carreras',
  templateUrl: './listado-carreras.component.html',
  styleUrls: ['./listado-carreras.component.css']
})
export class ListadoCarrerasComponent implements OnInit {

  listaCarreras: Carrera[];
  modalRef: NgbModalRef;
  _id: '';
  nombre_circuito = '';
  nombre_lugar: '';
  direccion: '';
  fecha: any;
  web: '';
  telefono: '';
  pais: '';
  foto_carrera: '';
  url_entrada: '';
  carreraSelected: Carrera;

  @Output() carreraEliminadaClicked: EventEmitter<Carrera> = new EventEmitter();

  constructor(private carrerasService: CarrerasService, private authService: AuthService,
              private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.authService.checkAuth();
    this.carrerasService.getCarreras().subscribe(
      carreras => {this.listaCarreras = carreras;
      console.log(carreras);
      }
    );
  }

  mostrarModal(modalContent: any, c: Carrera) {
    this.modalRef = this.modalService.open(modalContent);
    this.carreraSelected = c;
  }

  agregarCarrera(modalAgregarCarrera) {
    this.modalRef.dismiss(modalAgregarCarrera);
    this.carrerasService.addCarrera(this.nombre_circuito, this.nombre_lugar, this.direccion, this.fecha, this.web, this.telefono, this.pais, this.foto_carrera, this.url_entrada).subscribe( carrer => {
      location.reload();
      log('Añadido carrera nueva');
    }, error2 => {
        alert('Campos incorrectos');
      });
  }

  borrarCarrera(idCarrera: string, carrera: Carrera) {
    this.carrerasService.deleteCarrera(idCarrera).subscribe(carrer => {
      location.reload();
      log('Se ha borrado con exito la carrera');
      this.carreraSelected = carrer;
    });
  }

  editarCarrera(modalEditarCarrera) {
    this.carrerasService.editCarrera(this.carreraSelected._id, this.carreraSelected.nombre_circuito, this.carreraSelected.nombre_lugar, this.carreraSelected.direccion,
      this.carreraSelected.fecha, this.carreraSelected.web, this.carreraSelected.telefono, this.carreraSelected.pais, this.carreraSelected.foto_carrera, this.carreraSelected.url_entrada).subscribe( carrer => {
        this.carreraSelected = carrer;
        this.modalRef.dismiss(modalEditarCarrera);
    });
  }

}

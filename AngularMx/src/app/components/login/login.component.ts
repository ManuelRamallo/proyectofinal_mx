import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = '';
  password = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.checkLoggedIn();
  }

  login() {
    this.authService.doLogin(this.email, this.password).subscribe(
      user => {
        console.log('Login correcto');
        this.authService.setLoginData(user.token, user.nombre, user.apellidos);
        this.router.navigate(['/home']);
      },
        error => {
          alert('Login Incorrecto');
        }
    );
  }

}

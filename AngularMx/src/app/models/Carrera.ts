export class Carrera {
  public _id: string;
  public nombre_circuito: string;
  public nombre_lugar: string;
  public direccion: string;
  public fecha: any;
  public web: string;
  public telefono: string;
  public pais: string;
  public foto_carrera: string;
  public url_entrada: string;
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {User} from '../models/User';
import {Observable} from 'rxjs/Observable';

const requestOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json'
  })
};

@Injectable()
export class AuthService {

  authUrl = 'https://apimx-kwobrbcbjy.now.sh/api/v1/auth';

  constructor(private http: HttpClient, private router: Router) { }

  doLogin(email: string, password: string): Observable<User> {
    if (email !== 'admin@gmail.com') {
      alert('El usuario debe de ser un administrador');
    } else {
      return this.http.post<User>(`${this.authUrl}/login`,
        {email: email, password: password}, requestOptions);
    }
  }

  setLoginData(token: string, nombre: string, apellidos: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('nombre', nombre);
    localStorage.setItem('apellidos', apellidos);
  }

  checkAuth() {
    const token = localStorage.getItem('token');
    if ( token == null) {
      this.router.navigate(['/']);
    }
  }

  checkLoggedIn() {
    const token = localStorage.getItem('token');
    if (token != null) {
      this.router.navigate(['/home']);
    }
  }

  doLogout() {
    localStorage.removeItem('token');
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  getNombre(): string {
    return localStorage.getItem('nombre');
  }

  getApellidos(): string {
    return localStorage.getItem('apellidos');
  }



}

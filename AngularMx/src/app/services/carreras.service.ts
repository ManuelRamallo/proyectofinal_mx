import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs/Observable';
import {Carrera} from '../models/Carrera';

@Injectable()
export class CarrerasService {
  carrerasUrl = 'https://apimx-kwobrbcbjy.now.sh/api/v1/carreras';
  public requestOptions: HttpHeaders;
  token: string;

  constructor(private http: HttpClient, private authService: AuthService) {
    this.token = this.authService.getToken();
    this.requestOptions = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    });
  }

  getCarreras(): Observable<Carrera[]> {
    return this.http.get<Carrera[]>(`${this.carrerasUrl}/list`, {headers: this.requestOptions});
  }

  addCarrera(nombre_circuito: string, nombre_lugar: string, direccion: string,
             fecha: any, web: string, telefono: string, pais: string, foto_carrera: string, url_entrada: string): Observable<any> {
    return this.http.post<any>(`${this.carrerasUrl}/add`, {nombre_circuito: nombre_circuito, nombre_lugar: nombre_lugar,
    direccion: direccion, fecha: fecha, web: web, telefono: telefono, pais: pais, foto_carrera: foto_carrera, url_entrada: url_entrada}, {headers: this.requestOptions});
  }

  deleteCarrera(idCarrera): Observable<any> {
    return this.http.delete<any>(`${this.carrerasUrl}/delete/${idCarrera}`, {headers: this.requestOptions});
  }

  editCarrera(idCarrera: string, nombre_circuito: string, nombre_lugar: string, direccion: string,
              fecha: any, web: string, telefono: string, pais: string, foto_carrera: string, url_entrada: string): Observable<any> {
    return this.http.put<any>(`${this.carrerasUrl}/edit/${idCarrera}`, {nombre_circuito: nombre_circuito, nombre_lugar: nombre_lugar,
      direccion: direccion, fecha: fecha, web: web, telefono: telefono, pais: pais, foto_carrera: foto_carrera, url_entrada: url_entrada}, {headers: this.requestOptions});
  }

}

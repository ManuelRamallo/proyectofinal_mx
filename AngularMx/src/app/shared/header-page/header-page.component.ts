import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header-page',
  templateUrl: './header-page.component.html',
  styleUrls: ['./header-page.component.css']
})
export class HeaderPageComponent implements OnInit {

  nombre;
  apellidos;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.nombre = this.authService.getNombre();
    this.apellidos = this.authService.getApellidos();
  }

  logout() {
    this.authService.doLogout();
    this.router.navigate(['/login']);
  }

}

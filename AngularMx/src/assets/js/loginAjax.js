// 1. Definir evento click sobre el boton de login

$(document).on("click","#btn-login",function() {
  //Obtenemos lo que el usuario ha escrito en los input
  var e = $("#email").val();
  var p = $("#password").val();

  $.ajax({
    method: "POST",
    url: "http://localhost:8080/auth/login",
    headers: { email: e, password: p }
  })
  .done(function( respuesta ) {
    let key = respuesta.key;
    let nombre = respuesta.nombre;
    let email = respuesta.email;
    let clase = respuesta.clase;
    localStorage.setItem("key",key);
    localStorage.setItem("nombre",nombre);
    localStorage.setItem("clase", clase);
    //redirecciono al usuario a la pagina de inicio
    window.location.href = "index.html";
  })
  .fail(function( error){
    alert("login Incorrecto");
    console.log(error);
  });

});

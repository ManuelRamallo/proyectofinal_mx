$(document).ready(function() {
  var key = localStorage.getItem("key");
  var nombre = localStorage.getItem("nombre");
  var clase = localStorage.getItem("clase");

  var listaCampanyas;

  var listaCampanyasUnidas;
  var listaCampanyasUnidasId = [];

  var countLenght;
  var htmlCampanya;
  var id;
  var count;
  var countLenght;
  // MOSTRAR LISTA DE AVERÍAS
  var baseURL = "http://localhost:8080/";

  var codigo;

  $('#usuario').text(nombre);
  $('#clase').text(clase);

  //MÉTODO QUE RECOGE LAS CAMPAÑAS EN LAS CUALES EL USUARIO YA ESTÁ UNIDO
  function recuperarCampanyasUsuario() {
    $.ajax({
        method: "GET",
        url: `${baseURL}campaign/mylist`,
        headers: {
          "api_key": key
        }
      })
      .done(function(respuesta) {
        count = Object.keys(respuesta).length;

        if (count == 0) {
          listaCampanyasUnidas = 0;
        } else {
          listaCampanyasUnidas = respuesta;

          //Relleno con la lista con los ID de las campañas del usuario
          listaCampanyasUnidas.forEach(function(campanya) {
            listaCampanyasUnidasId.push(campanya.id);
          })
        }


      })
      .fail(function(error) {
        alert("fallo del servidor");
        console.log(error);
        window.location.href = "login.html"
      });
  }

  recuperarCampanyasUsuario();

  //MÉTODO QUE MUESTRA TODAS LAS CAMPAÑAS
  $.ajax({
      method: "GET",
      url: `${baseURL}campaign/list`,
      headers: {
        "api_key": key
      }
    })
    .done(function(respuesta) {

      listaCampanyas = respuesta;
      /*countLenght = Object.keys(listaCampanyas).length;
      console.log(countLenght);*/

      if (!listaCampanyasUnidas == 0) {
        listaCampanyas.forEach(function(campanya) {
        /*  var listaUsuarios = campanya.listaUsuarios;
          listaUsuarios = Object.keys(listaUsuarios).length;*/

          if (listaCampanyasUnidasId.includes(campanya.id)) {
            htmlCampanya = `
              <div class="col-lg-3 mb-3">
                <div class="card card-inverse card-primary campanya" id="${campanya.id}">
                  <div class="card-header nombre-campanya" id="${campanya.codigo}">${campanya.nombre}</div>

                  <button type="button" class="btn btn-sm btn-primary btn-unido">Ya estás unido</button>
                </div>
              </div>
              `;
          } else {
            htmlCampanya = `
              <div class="col-lg-3 mb-3">
                <div class="card card-inverse card-primary campanya" id="${campanya.id}">
                  <div class="card-header nombre-campanya" id="${campanya.codigo}">${campanya.nombre}</div>
                  <button type="button" class="btn btn-sm btn-primary btn-unir">Unirse</button>
                </div>
              </div>
              `;
          }

          $("#listadoCampanyas").append(htmlCampanya);
        });
      } else {
        listaCampanyas.forEach(function(campanya) {
          /*var listaUsuarios = campanya.listaUsuarios;
          listaUsuarios = Object.keys(listaUsuarios).length;*/

          htmlCampanya = `
          <div class="col-lg-3 mb-3">
            <div class="card card-inverse card-primary campanya" id="${campanya.id}">
              <div class="card-header nombre-campanya" id="${campanya.codigo}">${campanya.nombre}</div>

              <button type="button" class="btn btn-sm btn-primary btn-unir">Unirse</button>
            </div>
          </div>
          `;
          $("#listadoCampanyas").append(htmlCampanya);
        });

      }


    })
    .fail(function(error) {
      alert("fallo del servidor");
      console.log(error);
    });


  //MÉTODOS QUE MUESTRAN EL MODAL PARA UNIRSE A UNA CAMPAÑA Y REALIZAN LA FUNCIONALIDAD PARA QUE EL USUARIO PUEDA UNIRSE A ELLA
  $(document).on("click", ".btn-unir", () => {
    idCampanyaUnir = $(this).closest(".campanya").attr("id");
    $("#modalUnirCampanya").modal("show")
  });

  $(document).on("click", "#btn-guardar-join", () => {

    // Cierro el modal cuando pulsa en el botón guardar
    $("#modalUnirCampanya").modal("hide");
    $.ajax({
        method: "POST",
        url: `${baseURL}campaign/join`,
        headers: {
          "api_key": key,
          "codigo": $("#codigoCam").val()
        }
      })
      .done(function(respuesta) {
        window.location.href = "index.html"
      })
      .fail(function(error) {
        console.log("Error del servidor");
      });
  });

  //MÉTODO PARA CERRAR SESIÓN
  $(document).on("click", ".logout-button", () => {

    $.ajax({
        method: "POST",
        url: `${baseURL}auth/signout`,
        headers: {
          "api_key": key
        }
      })
      .done(function(respuesta) {
        window.location.href = "login.html"
      })
      .fail(function(error) {
        console.log("Error del servidor");
      });
  });


});

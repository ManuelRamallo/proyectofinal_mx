$(document).ready(function() {
  var key = localStorage.getItem("key");
  var nombre = localStorage.getItem("nombre");
  var clase = localStorage.getItem("clase");

  var listaDatosMaestros;
  var listaAportaciones;
  var rankingAportaciones;
  var cantidadAportada = 0;
  var baseURL = "http://localhost:8080/";

  var arrUsuarios = [];
  var id = 0;

  var nombreCampanya = localStorage.getItem("nombre_campanya");
  var idCampanya = localStorage.getItem("id_campanya");

  var codigo;

  $('#usuario').text(nombre);
  $('#clase').text(clase);

  $('#nombreCampanya').text(nombreCampanya);

  moment.locale('es');

  // Definición de un objeto de tipo Usuario
  function Usuario(id, nombre, clase, cantidadTotal) {
    this.id = id,
      this.nombre = nombre,
      this.clase = clase,
      this.cantidadTotal = cantidadTotal
  }

  //MÉTODO QUE MUESTRA LAS ÚLTIMAS 5 APORTACIONES DEL USUARIO EN LA CAMPAÑA
  function mostrarMisUltimasAportaciones() {
    //Mis últimas aportaciones
    $(".div-aportacion").text("");
    $(".div-aportacion").prepend(`<div class="card-header">Mis últimas aportaciones</div>
    <button type="button" class="btn btn-sm btn-primary" id="btn-ver-todas">Ver todas</button>

    <div class="card-block" id="listadoAportaciones">

    </div>

    <button type="button" class="btn btn-sm btn-primary" id="btn-aportar">Aportar</button>`)
    
    $.ajax({
        method: "GET",
        url: `${baseURL}campaign/datosgrupo`,
        headers: {
          "api_key": key,
          "id_campanya": idCampanya
        }
      })
      .done(function(respuesta) {
        //Guardo la respuesta JSON en una lista
        listaAportaciones = respuesta;

        //Recojo el tamaño de la lista que acabo de recuperar
        var count = Object.keys(respuesta).length;

        //Si la lista contiene más de 4 elementos, te muestra solo los 5 últimos
        if (count >= 5) {
          for (var i = 0; i < 5; i++) {
            aportacion = listaAportaciones[i];
            fAportacion = moment(aportacion.fechaAportacion).fromNow();

            var htmlAportacion = `<p class="aportacion" id="${aportacion.id}">${aportacion.cantidad}KG
            <span class="datoInsercion">${aportacion.datoMaestro.dato}</span>
            <span class="fechaInsercion">${fAportacion}
            <span class="fa fa-trash-o btn-borrar" aria-hidden="true"></span>
            </span>
            </p>`
            $("#listadoAportaciones").append(htmlAportacion);
          }
          //Si contiene menos de 5, te los muestra todos
        } else {
          listaAportaciones.forEach(function(aportacion) {
            fAportacion = moment(aportacion.fechaAportacion).fromNow();
            var htmlAportacion = `<p class="aportacion" id="${aportacion.id}">${aportacion.cantidad}KG
            <span class="datoInsercion">${aportacion.datoMaestro.dato}</span>
            <span class="fechaInsercion">${fAportacion}
            <span class="fa fa-trash-o btn-borrar" aria-hidden="true"></span>
            </span>
            </p>`
            $("#listadoAportaciones").append(htmlAportacion);
          })
        }

      })
      .fail(function(error) {
        alert("fallo del servidor");
        console.log(key);
        //window.location.href = "login.html"
      });
  }

  mostrarMisUltimasAportaciones();

  //MÉTODO QUE MUESTRA TODAS LAS APORTACIONES
  $(document).on("click", "#btn-ver-todas", function() {
    $(".div-aportacion").text("");
    $(".div-aportacion").prepend(`
    <div class="card-header">Mis aportaciones</div>
    <button type="button" class="btn btn-sm btn-primary" id="btn-ver-ultimas">Ver últimas</button>

    <div class="card-block" id="listadoAportaciones">

    </div>

    <button type="button" class="btn btn-sm btn-primary" id="btn-aportar">Aportar</button>`)
    $.ajax({
        method: "GET",
        url: `${baseURL}campaign/datosgrupo`,
        headers: {
          "api_key": key,
          "id_campanya": idCampanya
        }
      })
      .done(function(respuesta) {
        //Guardo la respuesta JSON en una lista
        listaAportaciones = respuesta;

        //Recoge todas las aportaciones y las muestra
        listaAportaciones.forEach(function(aportacion) {
          fAportacion = moment(aportacion.fechaAportacion).fromNow();
          var htmlAportacion = `<p class="aportacion" id="${aportacion.id}">${aportacion.cantidad}KG
          <span class="datoInsercion">${aportacion.datoMaestro.dato}</span>
          <span class="fechaInsercion">${fAportacion}
          <span class="fa fa-trash-o btn-borrar" aria-hidden="true"></span>
          </span>
          </p>`
          $("#listadoAportaciones").append(htmlAportacion);
        })
      }).fail(function(error) {
        console.log(key);
        alert("Y PETÓ")
      });
  });

  //MÉTODO QUE MUESTRA LAS ÚLTIMAS APORTACIONES HACIENDO CLICK EN EL BOTÓN DE VER ÚLTIMAS (CUANDO SE ESTÁ EN LA VISTA DE VER TODAS)
  $(document).on("click", "#btn-ver-ultimas", function() {
    mostrarMisUltimasAportaciones();
  });

  function compare(a, b) {
    if (a.cantidad < b.cantidad) {
      return -1;
    }
    if (a.cantidad > b.cantidad) {
      return 1;
    }
    // a debe ser igual b
    return 0;
  }


  $.ajax({
      method: "GET",
      url: `${baseURL}campaign/total`,
      headers: {
        "api_key": key,
        "id_campanya": idCampanya
      }
    })
    .done(function(respuesta) {
      //Guardo la respuesta JSON en una lista

      //TODO
      var listaUsuarios = respuesta;

      listaUsuarios.forEach(function(usuario) {
        var listaCantidadKilos = usuario.listaAportaciones
        //  console.log(listaCantidadKilos);
        listaCantidadKilos.forEach(function(aportacion) {
          if (aportacion.campanya.id == idCampanya) {
            cantidadAportada = cantidadAportada + aportacion.cantidad;
            //  console.log(cantidadAportada);
          }
        });
        arrUsuarios.push(new Usuario(usuario.id, usuario.nombre, usuario.clase, cantidadAportada));
      });

      arrUsuarios.sort(function(a, b) {
        if (a.cantidadTotal > b.cantidadTotal) {
          return -1;
        }
        if (a.cantidadTotal < b.cantidadTotal) {
          return 1;
        }
        // a must be equal to b
        return 0;
      });

      var count = Object.keys(arrUsuarios).length;
      console.log(count);

      if (count < 5) {
        arrUsuarios.forEach(function(usuario) {
          id++;
          var htmlRanking = `<p>#${id}- <span class="datoInsercion">${usuario.clase}</span> <span class="fechaInsercion">Total recaudado: ${usuario.cantidadTotal}KG</span></p>`
          $("#ranking").append(htmlRanking);
        })
      } else {
        for (var i = 0; i <= 5; i++) {
          var usuario = arrUsuarios[i];
          id++;
          var htmlRanking = `<p>#${id}- <span class="datoInsercion">${usuario.clase}</span> <span class="fechaInsercion">Total recaudado: ${usuario.cantidadTotal}KG</span></p>`
          $("#ranking").append(htmlRanking);
        }
      }


    }).fail(function(error) {
      console.log(key);
      alert("Y PETÓ")
    });

  //MÉTODO QUE MUESTRA TODAS LOS DATOS MAESTROS EN EL MODAL
  $.ajax({
      method: "GET",
      url: `${baseURL}campaign/datamaster`,
      headers: {
        "api_key": key,
        "id_campanya": idCampanya
      }
    })
    .done(function(respuesta) {

      listaDatosMaestros = respuesta;

      listaDatosMaestros.forEach(function(datoMaestro) {
        var htmlDatosSection = `<option class="dato-maestro" value="${datoMaestro.id}">${datoMaestro.dato}</option>`
        $("#listadoDatosMaestros").append(htmlDatosSection);
      })

    })
    .fail(function(error) {
      alert("fallo del servidor");
      console.log(key);
    });

  //MÉTODO QUE ABRE EL MODAL DE BORRAR APORTACIÓN
  $(document).on("click", ".btn-borrar", function() {
    $("#modalBorrar").modal("show");
    idAportacion = $(this).closest(".aportacion").attr("id");
  });

  //MÉTODO QUE BORRA UNA APORTACIÓN CUANDO SE LE DA AL BOTÓN ACEPTAR DEL MODAL
  $(document).on("click", "#btn-aceptar", function() {
    $("#modalBorrar").modal("hide");

    $.ajax({
        method: "DELETE",
        url: `${baseURL}campaign/delete`,
        headers: {
          "api_key": key,
          "id_campanya": idCampanya,
          "id_aportacion": idAportacion
        }
      })
      .done(function(respuesta) {
        location.reload();
      })
      .fail(function(error) {
        console.log("Error del servidor");
        alert("Y PETÓ")
      });
  });

  //MÉTODO QUE CIERRA EL MODAL SIN BORRAR LA APORTACION SI EL USUARIO PULSA CANCELAR
  $(document).on("click", "#btn-cancelar", function() {
    $("#modalBorrar").modal("hide");
  });


  //MÉTODO QUE ABRE EL MODAL UNA VEZ SE HAGA CLICK EN EL BOTON DE APORTAR
  $(document).on("click", "#btn-aportar", function() {
    $("#modalAportacion").modal("show");
  });

  //MÉTODO QUE GUARDA LOS DATOS INTRODUCIDOS POR EL USUARIO EN EL MODAL
  $(document).on("click", "#btn-guardar-anyadir", function() {

    idDatoMaestro = $("#listadoDatosMaestros").val();

    $("#modalAportacion").modal("hide");
    $.ajax({
        method: "POST",
        url: `${baseURL}campaign/add`,
        headers: {
          "api_key": key,
          "cantidad": $("#cantidad").val(),
          "id_campanya": idCampanya,
          "id_dato": idDatoMaestro
        }
      })
      .done(function(respuesta) {
        location.reload();
      })
      .fail(function(error) {
        console.log("Error del servidor");
      });
  });

  //MÉTODO PARA CERRAR SESIÓN
  $(document).on("click", ".logout-button", function() {

    $.ajax({
        method: "POST",
        url: `${baseURL}auth/signout`,
        headers: {
          "api_key": key
        }
      })
      .done(function(respuesta) {
        window.location.href = "login.html"
      })
      .fail(function(error) {
        console.log("Error del servidor");
      });
  });


});

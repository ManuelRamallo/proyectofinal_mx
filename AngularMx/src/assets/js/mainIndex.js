$(document).ready(function() {
  var listaCampanyas;
  // MOSTRAR LISTA DE AVERÍAS
  var baseURL = "http://localhost:8080/";

  var key = localStorage.getItem("key");
  var nombre = localStorage.getItem("nombre");
  var clase = localStorage.getItem("clase");

  var codigo;

  $('#usuario').text(nombre);
  $('#clase').text(clase);

//MÉTODO QUE RECATA LAS CAMPAÑAS (SI HUBIERA) EN LAS CUALES EL USUARIO ESTÁ UNIDO
  $.ajax({
      method: "GET",
      url: `${baseURL}campaign/mylist`,
      headers: {
        "api_key": key
      }
    })
    .done(function(respuesta) {
      listaCampanyas = respuesta;

      listaCampanyas.forEach(function(campanya) {
      /*  var listaUsuarios = campanya.listaUsuarios;
        listaUsuarios = Object.keys(listaUsuarios).length;*/
        var htmlCampanya = `
          <div class="col-lg-3 mb-3">
            <div class="card card-inverse card-primary campanya" id="${campanya.id}">
              <div class="card-header nombre-campanya" id="${campanya.codigo}">${campanya.nombre}</div>

                <button type="button" class="btn btn-sm btn-primary btn-info">+ Info</button>
            </div>
          </div>
          `;

        $("#listadoCampanyas").append(htmlCampanya);
      });
    })
    .fail(function(error) {
      alert("fallo del servidor");
      console.log(error);
      window.location.href = "login.html"
    });

  //MÉTODO PARA RESCATAR EL ID DE UNA CAMPAÑA Y MOSTRAR SUS DETALLES
  $(document).on("click", ".btn-info", function() {

    let idCampanya = $(this).closest(".campanya").attr("id");
    localStorage.setItem("id_campanya", idCampanya);

    let nombreCampanya = $(this).closest(".campanya").find(".nombre-campanya").text();
    localStorage.setItem("nombre_campanya", nombreCampanya);

    window.location.href = "detallesCampanya.html";

  });
  //MÉTODO PARA CERRAR SESIÓN
  $(document).on("click", ".logout-button", () => {

    $.ajax({
        method: "POST",
        url: `${baseURL}auth/signout`,
        headers: {
          "api_key": key
        }
      })
      .done(function(respuesta) {
        window.location.href = "login.html"
      })
      .fail(function(error) {
        console.log("Error del servidor");
      });
  });

});

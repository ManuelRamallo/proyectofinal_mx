

// 1. Definir evento click sobre el Botón de login

$(document).on("click","#btnRegistro",() =>{
  // Obtenemos lo que el usuario ha escrito en los input Email y Password
  // Para obtener el valor que se ha escrito en un input, utilizamos la función .val()
  var e = $("#inputEmail").val();
  var n = $("#inputNombre").val();
  var p = $("#inputPassword").val();

  $.ajax({
    method: "POST",
    url: "http://miguelcamposrivera.com/mecaround/api/v1/auth/register",
    data: { email: e, password: p, nombre: n }
  })
  .done(function(respuesta) {
    // respuesta > es un objeto JSON que recibo del servidor
    let key = respuesta.key;
    let nombre = respuesta.nombre;
    let email = respuesta.email;

    console.log(`${key}, ${nombre}, ${email}`);

    // Redirecciono al usuario a la página de inicio
    window.location.href = "index.html";
  })
  .fail(function(error){
    alert("Login incorrecto");
    console.log(error);
  });

});

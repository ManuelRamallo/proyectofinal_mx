$(document).on("click", "#btn-register", function() {

  var e = $("#email").val();
  var n = $("#nombre").val();
  var p = $("#password").val();
  var c = $("#clase").val();

  $.ajax({
      method: "POST",
      url: "http://localhost:8080/auth/register",
      headers: {
        email: e,
        password: p,
        nombre: n,
        clase: c
      }
    })
    .done(function(respuesta) {
      // respuesta > es un objeto JSON que recibo del servidor
      let key = respuesta.key;
      let nombre = respuesta.nombre;
      let email = respuesta.email;
      let clase = respuesta.clase;

      console.log(`${key}, ${nombre}, ${email}, ${clase}`);

      localStorage.setItem("key", key);
      localStorage.setItem("nombre", nombre);
      localStorage.setItem("clase", clase);
      // Redirecciono al usuario a la página de inicio
      window.location.href = "campanyas.html";
    })
    .fail(function(error) {
      alert("Login incorrecto");
      console.log(error);
    });

});

package com.mrdiaz.mx.API;

import com.mrdiaz.mx.models.Carreras;
import com.mrdiaz.mx.models.Pilotos;
import com.mrdiaz.mx.models.User;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by ridex on 19/03/2018.
 */

public interface InterfaceRequestApi {

    @POST("api/v1/auth/login")
    public Call<User> loginUser(@Body User userLoged);

    @POST("api/v1/auth/register")
    public Call<User> registerUser(@Body User newUser);

    @GET("api/v1/carreras/list")
    public Call<List<Carreras>> findAllCarreras(@Header("Authorization") String token);

    @GET("api/v1/pilotos/list")
    public Call<List<Pilotos>> findAllPilotos(@Header("Authorization") String token);

    @GET("api/v1/carreras/detalles/{id}")
    public Call<Carreras> detallesCarreras(@Header("Authorization") String token, @Path("id") String id_carrera);

    @GET("api/v1/pilotos/detalles/{id}")
    public Call<Pilotos> detallesPilotos(@Header("Authorization") String token, @Path("id") String id_piloto);

    @GET("api/v1/auth/detalles/{id}")
    public Call<User> detallesUsuario(@Header("Authorization")String token, @Path("id") String id_usuario);

    @Multipart
    @PUT("/api/v1/auth/edit/{id}")
    Call<ResponseBody> editarUsuario(@Header("Authorization") String token, @Path("id") String id_usuario, @Part("nombre") RequestBody nombre, @Part("apellidos") RequestBody apellidos, @Part("email") RequestBody email, @Part("password") RequestBody password , @Part MultipartBody.Part photo);


}

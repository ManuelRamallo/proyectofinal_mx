package com.mrdiaz.mx.Constant;

/**
 * Created by ridex on 19/03/2018.
 */

public interface PreferencesKeys {

    final String USER_NAME = "userName";
    final String USER_SURNAME = "userSurname";
    final String USER_EMAIL = "userEmail";
    final String USER_PASSWORD = "userPassword";
    final String USER_PHOTO = "userPhoto";
    final String USER_TOKEN = "userToken";
    final String USER_ID = "user_id";


}

package com.mrdiaz.mx.activities;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.mrdiaz.mx.R;
import com.mrdiaz.mx.fragments.CarrerasFragment;
import com.mrdiaz.mx.fragments.DialogLogOut;
import com.mrdiaz.mx.fragments.IOnCarreraInteractionListener;
import com.mrdiaz.mx.fragments.IOnPilotoInteractionListener;
import com.mrdiaz.mx.fragments.IOnUsuarioInteractionListener;
import com.mrdiaz.mx.fragments.PilotosFragment;
import com.mrdiaz.mx.fragments.UsuarioFragment;
import com.mrdiaz.mx.models.Carreras;
import com.mrdiaz.mx.models.Pilotos;
import com.mrdiaz.mx.models.User;

public class CarrerasActivity extends AppCompatActivity implements IOnCarreraInteractionListener, IOnPilotoInteractionListener, IOnUsuarioInteractionListener{


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        Fragment fragment = null;

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new CarrerasFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, fragment).commit();
                    return true;
                case R.id.navigation_pilotos:
                    fragment = new PilotosFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, fragment).commit();
                    return true;
                case R.id.navigation_perfil:
                    fragment = new UsuarioFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, fragment).commit();
                    return true;
            }

            if(fragment == null){
                getSupportFragmentManager().beginTransaction().add(R.id.contenedor, new CarrerasFragment()).commit();
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carreras);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getSupportFragmentManager().beginTransaction().add(R.id.contenedor, new CarrerasFragment()).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_logout:
                FragmentManager fragmentManager = getFragmentManager();
                DialogLogOut dialogLogOut = new DialogLogOut();

                dialogLogOut.show(fragmentManager, "tagLogOut");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.opciones, menu);
        return true;
    }


    @Override
    public void onCarreraClick(Carreras carrera) {
        Intent intentCarrera = new Intent(this, DetallesCarreraActivity.class);
        intentCarrera.putExtra("id_carrera", carrera.get_id());
        startActivity(intentCarrera);
    }

    @Override
    public void onPilotoClick(Pilotos piloto) {
        Intent intentPiloto = new Intent(this, DetallesPilotoActivity.class);
        intentPiloto.putExtra("id_piloto", piloto.get_id());
        startActivity(intentPiloto);
    }

    @Override
    public void onUsuarioClick(User usuario) {

    }
}

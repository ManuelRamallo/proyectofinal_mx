package com.mrdiaz.mx.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mrdiaz.mx.API.InterfaceRequestApi;
import com.mrdiaz.mx.API.ServiceGenerator;
import com.mrdiaz.mx.Constant.PreferencesKeys;
import com.mrdiaz.mx.R;
import com.mrdiaz.mx.models.Carreras;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetallesCarreraActivity extends AppCompatActivity {

    TextView textViewNombreCircuito, textViewDireccionCarrera, textViewFechaCarrera, textViewTelefonoCarrera, textViewWebCarrera;
    ImageView imageViewFotoCarrera, imageViewPaisCarrera;
    Button btEntrada;
    SharedPreferences prefs;
    String token;
    InterfaceRequestApi api;
    Carreras detallesCarrera;
    Context ctx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_carrera);

        textViewNombreCircuito = findViewById(R.id.nombre_circuito_carrera);
        textViewDireccionCarrera = findViewById(R.id.direccion_carrera);
        textViewFechaCarrera = findViewById(R.id.fecha_carrera);
        textViewTelefonoCarrera = findViewById(R.id.telefono_carrera);
        textViewWebCarrera = findViewById(R.id.web_carrera);
        imageViewFotoCarrera = findViewById(R.id.foto_carrera);
        imageViewPaisCarrera = findViewById(R.id.pais_carrera);
        btEntrada = findViewById(R.id.bt_entrada);


        setTitle(getString(R.string.titulo));

        Bundle extras = DetallesCarreraActivity.this.getIntent().getExtras();

        String id_carrera = extras.getString("id_carrera");


        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(DetallesCarreraActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage(getString(R.string.mensaje_dialog_detallesCarrera));
        progressDoalog.setTitle(getString(R.string.titulo_dialog_detallesCarrera));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

        prefs = DetallesCarreraActivity.this.getSharedPreferences("datos", Context.MODE_PRIVATE);
        token = prefs.getString(PreferencesKeys.USER_TOKEN, null);

        api = ServiceGenerator.createService(InterfaceRequestApi.class);

        Call<Carreras> call = api.detallesCarreras("Bearer " + token, id_carrera);

        call.enqueue(new Callback<Carreras>() {
            @Override
            public void onResponse(Call<Carreras> call, Response<Carreras> response) {

                progressDoalog.dismiss();
                
                if(response.isSuccessful()){
                    detallesCarrera = response.body();

                    btEntrada.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Uri uri = Uri.parse(detallesCarrera.getUrl_entrada());
                            Intent intentEntrada = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intentEntrada);
                        }
                    });

                    textViewNombreCircuito.setText(detallesCarrera.getNombre_circuito());
                    textViewDireccionCarrera.setText(detallesCarrera.getDireccion());
                    textViewFechaCarrera.setText(detallesCarrera.getFecha());
                    textViewTelefonoCarrera.setText(detallesCarrera.getTelefono());
                    textViewWebCarrera.setText(detallesCarrera.getWeb());

                    Picasso.with(ctx)
                            .load(detallesCarrera.getFoto_carrera())
                            .resize(1300, 700)
                            .centerCrop()
                            .into(imageViewFotoCarrera);

                    Picasso.with(ctx)
                            .load(detallesCarrera.getPais())
                            .resize(120, 80)
                            .centerCrop()
                            .into(imageViewPaisCarrera);

                } else {
                    Toast.makeText(DetallesCarreraActivity.this, getString(R.string.toast_detallesCarrera), Toast.LENGTH_SHORT).show();
                }
                
            }

            @Override
            public void onFailure(Call<Carreras> call, Throwable t) {
                Toast.makeText(DetallesCarreraActivity.this, getString(R.string.fallo_conexion), Toast.LENGTH_SHORT).show();
            }
        });


    }
}

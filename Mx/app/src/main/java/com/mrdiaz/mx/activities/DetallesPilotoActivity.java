package com.mrdiaz.mx.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mrdiaz.mx.API.InterfaceRequestApi;
import com.mrdiaz.mx.API.ServiceGenerator;
import com.mrdiaz.mx.Constant.PreferencesKeys;
import com.mrdiaz.mx.R;
import com.mrdiaz.mx.fragments.CircleTransform;
import com.mrdiaz.mx.models.Pilotos;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetallesPilotoActivity extends AppCompatActivity {


    TextView textViewNombrePiloto, textViewDorsalPiloto, textViewEquipoPiloto, textViewFechaPiloto, textViewPrimerGpPiloto, textViewTitulosMundialesPilotos, textViewTitulosGPPiloto;
    ImageView imageViewFotoPiloto, imageViewNacionalidadPiloto;
    SharedPreferences prefs;
    String token;
    InterfaceRequestApi api;
    Pilotos detallesPilotos;
    Context ctx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_piloto);

        textViewNombrePiloto = findViewById(R.id.nombre_piloto_detalles);
        textViewDorsalPiloto = findViewById(R.id.dorsal_piloto);
        textViewEquipoPiloto = findViewById(R.id.equipo_piloto);
        textViewFechaPiloto = findViewById(R.id.fecha_piloto);
        textViewPrimerGpPiloto = findViewById(R.id.primerGp_piloto);
        textViewTitulosMundialesPilotos = findViewById(R.id.titulosMundiales_piloto);
        textViewTitulosGPPiloto = findViewById(R.id.titulosGP_piloto);

        imageViewFotoPiloto = findViewById(R.id.ivFoto_piloto);
        imageViewNacionalidadPiloto = findViewById(R.id.ivNacionalidad);


        setTitle(getString(R.string.titulo_detallesPiloto));

        Bundle extras = DetallesPilotoActivity.this.getIntent().getExtras();

        String id_piloto = extras.getString("id_piloto");

        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(DetallesPilotoActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage(getString(R.string.mensaje_dialog_detallesCarrera));
        progressDoalog.setTitle(getString(R.string.titulo_dialog_detallesPiloto));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

        prefs = DetallesPilotoActivity.this.getSharedPreferences("datos", Context.MODE_PRIVATE);
        token = prefs.getString(PreferencesKeys.USER_TOKEN, null);

        api = ServiceGenerator.createService(InterfaceRequestApi.class);

        Call<Pilotos> call = api.detallesPilotos("Bearer " + token, id_piloto);
        
        call.enqueue(new Callback<Pilotos>() {
            @Override
            public void onResponse(Call<Pilotos> call, Response<Pilotos> response) {
                
                progressDoalog.dismiss();
                
                if(response.isSuccessful()){
                    detallesPilotos = response.body();
                    
                    textViewNombrePiloto.setText(detallesPilotos.getNombre_completo());
                    textViewFechaPiloto.setText(detallesPilotos.getEdad());
                    textViewDorsalPiloto.setText(detallesPilotos.getDorsal());
                    textViewEquipoPiloto.setText(detallesPilotos.getEquipo());
                    textViewPrimerGpPiloto.setText(detallesPilotos.getPrimer_gp());
                    textViewTitulosMundialesPilotos.setText(detallesPilotos.getTitulos_mundiales());
                    textViewTitulosGPPiloto.setText(detallesPilotos.getTitulos_gp());

                    Picasso.with(ctx)
                            .load(detallesPilotos.getFoto_piloto())
                            .resize(1300, 700)
                            .centerCrop()
                            .into(imageViewFotoPiloto);

                    Picasso.with(ctx)
                            .load(detallesPilotos.getNacionalidad())
                            .resize(120, 80)
                            .centerCrop()
                            .into(imageViewNacionalidadPiloto);
                } else {
                    Toast.makeText(DetallesPilotoActivity.this, getString(R.string.toast_detallesPiloto), Toast.LENGTH_SHORT).show();
                }
                
            }

            @Override
            public void onFailure(Call<Pilotos> call, Throwable t) {
                Toast.makeText(DetallesPilotoActivity.this, getString(R.string.fallo_conexion), Toast.LENGTH_SHORT).show();
            }
        });


    }
}

package com.mrdiaz.mx.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mrdiaz.mx.API.InterfaceRequestApi;
import com.mrdiaz.mx.API.ServiceGenerator;
import com.mrdiaz.mx.Constant.PreferencesKeys;
import com.mrdiaz.mx.R;
import com.mrdiaz.mx.models.User;

import java.io.Console;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    TextInputEditText etCorreo, etPass;
    Button btRegistro, btLogin;
    ProgressBar progressBarCarga;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btLogin = findViewById(R.id.bt_login);
        btRegistro = findViewById(R.id.bt_registro);
        etCorreo = findViewById(R.id.etCorreo);
        etPass = findViewById(R.id.etPass);
        progressBarCarga = findViewById(R.id.progressBarCarga);

        progressBarCarga.setVisibility(View.INVISIBLE);

        eventListener();

    }

    private void eventListener() {
        btLogin.setOnClickListener(this);
        btRegistro.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int seleccion = v.getId();

        switch (seleccion){
            case R.id.bt_registro:
                Intent intentRegistro = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(intentRegistro);
                break;

            case R.id.bt_login:
                User userLoged = new User();

                prefs = LoginActivity.this.getSharedPreferences("datos", Context.MODE_PRIVATE);
                editor = prefs.edit();

                progressBarCarga.setVisibility(View.VISIBLE);

                userLoged.setEmail(etCorreo.getText().toString());
                userLoged.setPassword(etPass.getText().toString());

                InterfaceRequestApi api = ServiceGenerator.createService(InterfaceRequestApi.class);

                Call<User> call = api.loginUser(userLoged);

                if(etCorreo.getText().toString().equals("")){
                    etCorreo.setError(getString(R.string.error_correo));
                } else if (etPass.getText().toString().equals("")){
                    etPass.setError(getString(R.string.error_password));
                } else {


                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                User result = response.body();

                                progressBarCarga.setVisibility(View.INVISIBLE);

                                editor.putString(PreferencesKeys.USER_NAME, result.getNombre());
                                editor.putString(PreferencesKeys.USER_SURNAME, result.getApellidos());
                                editor.putString(PreferencesKeys.USER_EMAIL, result.getEmail());
                                editor.putString(PreferencesKeys.USER_PASSWORD, result.getPassword());
                                editor.putString(PreferencesKeys.USER_PHOTO, result.getPhoto());
                                editor.putString(PreferencesKeys.USER_TOKEN, result.getToken());
                                editor.putString(PreferencesKeys.USER_ID, result.get_id());


                                editor.commit();

                                Intent intentLogin = new Intent(LoginActivity.this, CarrerasActivity.class);
                                startActivity(intentLogin);
                                finish();
                            } else {
                                Toast.makeText(LoginActivity.this, getString(R.string.fallo_toast_login), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, getString(R.string.fallo_conexion), Toast.LENGTH_SHORT).show();
                            progressBarCarga.setVisibility(View.INVISIBLE);
                        }
                    });

                }

        }

    }
}

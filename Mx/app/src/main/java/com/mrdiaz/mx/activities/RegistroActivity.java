package com.mrdiaz.mx.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mrdiaz.mx.API.InterfaceRequestApi;
import com.mrdiaz.mx.API.ServiceGenerator;
import com.mrdiaz.mx.Constant.PreferencesKeys;
import com.mrdiaz.mx.R;
import com.mrdiaz.mx.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputEditText email, password, nombre, apellidos;
    Button btRegistro;

    ProgressBar progressBarCarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        btRegistro = findViewById(R.id.bt_registro);
        email = findViewById(R.id.email_registro);
        password= findViewById(R.id.password_registro);
        nombre = findViewById(R.id.nombre_registro);
        apellidos = findViewById(R.id.apellidos_registro);
        progressBarCarga = findViewById(R.id.progressBarCargaRegistro);

        progressBarCarga.setVisibility(View.INVISIBLE);

        eventListener();

    }

    private void eventListener() {
        btRegistro.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int seleccion = v.getId();

        switch (seleccion) {
            case R.id.bt_registro:
                if(nombre.getText().toString().equals("")){
                    nombre.setError(getString(R.string.error_nombre));
                } else if(apellidos.getText().toString().equals("")){
                    apellidos.setError(getString(R.string.error_apellidos));
                } else if(email.getText().toString().equals("")){
                    email.setError(getString(R.string.error_correo));
                } else if(password.getText().toString().equals("")){
                    password.setError(getString(R.string.error_password));
                } else {
                    SharedPreferences prefs = RegistroActivity.this.getSharedPreferences("datos", Context.MODE_PRIVATE);
                    final SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(PreferencesKeys.USER_NAME, nombre.getText().toString());
                    editor.putString(PreferencesKeys.USER_SURNAME, apellidos.getText().toString());
                    editor.putString(PreferencesKeys.USER_EMAIL, email.getText().toString());
                    editor.putString(PreferencesKeys.USER_PASSWORD, password.getText().toString());

                    User userRegistered = new User();

                    progressBarCarga.setVisibility(View.VISIBLE);

                    userRegistered.setNombre(nombre.getText().toString());
                    userRegistered.setApellidos(apellidos.getText().toString());
                    userRegistered.setEmail(email.getText().toString());
                    userRegistered.setPassword(password.getText().toString());

                    InterfaceRequestApi api = ServiceGenerator.createService(InterfaceRequestApi.class);

                    Call<User> call = api.registerUser(userRegistered);

                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            User result = response.body();

                            if(response.isSuccessful()){

                                editor.putString(PreferencesKeys.USER_TOKEN, result.getToken());
                                editor.commit();

                                progressBarCarga.setVisibility(View.INVISIBLE);

                                Intent intentRegistro = new Intent(RegistroActivity.this, CarrerasActivity.class);
                                startActivity(intentRegistro);
                                finish();

                            } else {
                                Toast.makeText(RegistroActivity.this, getString(R.string.error_toast_registro), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Toast.makeText(RegistroActivity.this, getString(R.string.fallo_conexion), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
        }

    }
}

package com.mrdiaz.mx.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mrdiaz.mx.API.InterfaceRequestApi;
import com.mrdiaz.mx.API.ServiceGenerator;
import com.mrdiaz.mx.Constant.PreferencesKeys;
import com.mrdiaz.mx.R;
import com.mrdiaz.mx.activities.CarrerasActivity;
import com.mrdiaz.mx.models.Carreras;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarrerasFragment extends Fragment {

    private int mColumnCount = 1;
    List<Carreras> carrerasList;
    MyCarrerasRecyclerViewAdapter adapter;
    IOnCarreraInteractionListener mListener;
    RecyclerView recyclerView;
    SharedPreferences prefs;
    String token;
    SharedPreferences.Editor editor;
    InterfaceRequestApi api;

    public CarrerasFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_carreras_list, container, false);


        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            final ProgressDialog progressDoalog;
            progressDoalog = new ProgressDialog(getActivity());
            progressDoalog.setMax(100);
            progressDoalog.setMessage(getString(R.string.mensaje_dialog_detallesCarrera));
            progressDoalog.setTitle(getString(R.string.titulo_dialog_carrera));
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            // show it
            progressDoalog.show();

            prefs = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);

            token = prefs.getString(PreferencesKeys.USER_TOKEN, null);

            api = ServiceGenerator.createService(InterfaceRequestApi.class);

            Call<List<Carreras>> call = api.findAllCarreras("Bearer " + token);

            call.enqueue(new Callback<List<Carreras>>() {
                @Override
                public void onResponse(Call<List<Carreras>> call, Response<List<Carreras>> response) {

                    progressDoalog.dismiss();

                    if (response.isSuccessful()) {
                        carrerasList = response.body();

                        adapter = new MyCarrerasRecyclerViewAdapter(getActivity(), carrerasList, mListener);
                        recyclerView.setAdapter(adapter);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.toast_carreraFragment), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<Carreras>> call, Throwable t) {
                    Toast.makeText(getActivity(), getString(R.string.fallo_conexion), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return view;
    }

   @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof IOnCarreraInteractionListener){
            mListener = (IOnCarreraInteractionListener) context;
        } else {
            throw new RuntimeException();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}

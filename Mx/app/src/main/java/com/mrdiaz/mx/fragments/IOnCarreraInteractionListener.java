package com.mrdiaz.mx.fragments;

import com.mrdiaz.mx.models.Carreras;

/**
 * Created by ridex on 19/03/2018.
 */

public interface IOnCarreraInteractionListener {

    void onCarreraClick(Carreras carrera);
}

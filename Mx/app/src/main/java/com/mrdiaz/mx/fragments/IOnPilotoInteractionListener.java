package com.mrdiaz.mx.fragments;

import com.mrdiaz.mx.models.Pilotos;

/**
 * Created by mrdiaz on 20/03/2018.
 */

public interface IOnPilotoInteractionListener {

    void onPilotoClick(Pilotos piloto);

}

package com.mrdiaz.mx.fragments;

import com.mrdiaz.mx.models.User;

/**
 * Created by mrdiaz on 21/03/2018.
 */

public interface IOnUsuarioInteractionListener {

    void onUsuarioClick(User usuario);
}

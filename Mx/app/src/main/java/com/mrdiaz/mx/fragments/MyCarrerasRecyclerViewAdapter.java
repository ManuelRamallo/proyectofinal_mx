package com.mrdiaz.mx.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrdiaz.mx.R;
import com.mrdiaz.mx.models.Carreras;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyCarrerasRecyclerViewAdapter extends RecyclerView.Adapter<MyCarrerasRecyclerViewAdapter.ViewHolder> {

    private Context ctx;
    private IOnCarreraInteractionListener mListener;
    private final List<Carreras> mValues;

    public MyCarrerasRecyclerViewAdapter(Context context, List<Carreras> items, IOnCarreraInteractionListener listener) {
        ctx = context;
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_carreras, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.textViewNombreLugar.setText(holder.mItem.getNombre_lugar());
        holder.textViewNombreCircuito.setText(holder.mItem.getNombre_circuito());
        holder.textViewFecha.setText(holder.mItem.getFecha());

        Picasso.with(ctx)
                .load(holder.mItem.getPais())
                .resize(120, 80)
                .centerCrop()
                .into(holder.foto_pais);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCarreraClick(holder.mItem);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView textViewNombreLugar;
        public final TextView textViewNombreCircuito;
        public final TextView textViewFecha;
        public final ImageView foto_pais;
        public Carreras mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            textViewNombreLugar = view.findViewById(R.id.nombre_lugar);
            textViewNombreCircuito = view.findViewById(R.id.nombre_circuito_carrera);
            textViewFecha = view.findViewById(R.id.fecha_carrera);
            foto_pais = view.findViewById(R.id.foto_pais);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + textViewNombreCircuito + "'";
        }
    }
}

package com.mrdiaz.mx.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrdiaz.mx.R;
import com.mrdiaz.mx.models.Pilotos;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyPilotosRecyclerViewAdapter extends RecyclerView.Adapter<MyPilotosRecyclerViewAdapter.ViewHolder> {

    private Context ctx;
    private final List<Pilotos> mValues;
    private final IOnPilotoInteractionListener mListener;

    public MyPilotosRecyclerViewAdapter(Context context, List<Pilotos> items, IOnPilotoInteractionListener listener) {
        ctx = context;
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_pilotos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.textViewNombrePiloto.setText(holder.mItem.getNombre_completo());
        holder.textViewEquipoPiloto.setText(holder.mItem.getEquipo());
        holder.textViewDorsalPiloto.setText(holder.mItem.getDorsal());

        Picasso.with(ctx)
                .load(holder.mItem.getNacionalidad())
                .resize(120, 80)
                .centerCrop()
                .into(holder.nacionalidadPiloto);

        Picasso.with(ctx)
                .load(holder.mItem.getFoto_piloto_perfil())
                .transform(new CircleTransform())
                .resize(200, 160)
                .centerCrop()
                .into(holder.fotoPiloto);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPilotoClick(holder.mItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView textViewNombrePiloto;
        public final TextView textViewEquipoPiloto;
        public final TextView textViewDorsalPiloto;
        public final ImageView fotoPiloto;
        public final ImageView nacionalidadPiloto;
        public Pilotos mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            textViewNombrePiloto = view.findViewById(R.id.nombre_piloto_detalles);
            textViewEquipoPiloto = view.findViewById(R.id.equipo_piloto);
            textViewDorsalPiloto = view.findViewById(R.id.dorsal_piloto);
            fotoPiloto = view.findViewById(R.id.foto_carrera);
            nacionalidadPiloto = view.findViewById(R.id.pais_carrera);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + textViewNombrePiloto + "'";
        }
    }
}

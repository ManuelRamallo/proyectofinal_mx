package com.mrdiaz.mx.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mrdiaz.mx.API.InterfaceRequestApi;
import com.mrdiaz.mx.API.ServiceGenerator;
import com.mrdiaz.mx.Constant.PreferencesKeys;
import com.mrdiaz.mx.R;
import com.mrdiaz.mx.models.User;
import com.squareup.picasso.Picasso;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;


public class UsuarioFragment extends Fragment {



   private IOnUsuarioInteractionListener mListener;
    SharedPreferences prefs;
    String token;
    String _id;
    SharedPreferences.Editor editor;
    InterfaceRequestApi api;
    Context ctx;

    public static final int PICK_IMAGE = 1;
    Uri selectedImage = null;

    TextView textViewNombreUsuario, textViewApellidosUsuario, textViewCorreoUsuario;
    ImageView imageViewFotoUsuario, btEditarFoto;


    public UsuarioFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view =  inflater.inflate(R.layout.fragment_usuario, container, false);

        prefs = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);

        token = prefs.getString(PreferencesKeys.USER_TOKEN, null);

        _id = prefs.getString(PreferencesKeys.USER_ID, null);

        api = ServiceGenerator.createService(InterfaceRequestApi.class);

        textViewNombreUsuario = view.findViewById(R.id.nombre_usuario);
        textViewApellidosUsuario = view.findViewById(R.id.apellidos_usuario);
        textViewCorreoUsuario = view.findViewById(R.id.correo_usuario);
        imageViewFotoUsuario = view.findViewById(R.id.fotoUsuario);

        btEditarFoto = view.findViewById(R.id.bt_editarFoto);

        btEditarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mayRequestStoragePermission(view);
                pickFromGal();


                if(selectedImage != null){

                    String strFile = FilesUtils.getFilePath(getActivity(), selectedImage);
                    File file = new File(strFile);

                    RequestBody requestFile = RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(selectedImage)), file
                    );

                    MultipartBody.Part body = MultipartBody.Part.createFormData(PreferencesKeys.USER_PHOTO, file.getName(), requestFile);
                    RequestBody nombre = RequestBody.create(MultipartBody.FORM, PreferencesKeys.USER_NAME);
                    RequestBody apellidos = RequestBody.create(MultipartBody.FORM, PreferencesKeys.USER_SURNAME);
                    RequestBody email = RequestBody.create(MultipartBody.FORM, PreferencesKeys.USER_EMAIL);
                    RequestBody password = RequestBody.create(MultipartBody.FORM, PreferencesKeys.USER_PASSWORD);



                    Call<ResponseBody> call = api.editarUsuario("Bearer " + token, _id, nombre, apellidos, email, password, body);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()){
                                Log.d("Upload", "Exito");
                                Log.d("Upload", response.body().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d("TAG", t.getMessage());
                        }
                    });

                }

            }
        });


        /*final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMax(100);
        progressDoalog.setMessage(getString(R.string.mensaje_dialog_detallesCarrera));
        progressDoalog.setTitle(getString(R.string.titulo_dialog_usuario));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();*/

        Call<User> call = api.detallesUsuario("Bearer " + token, _id);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

               /* progressDoalog.dismiss();*/

                if(response.isSuccessful()){
                    User result = response.body();

                    textViewNombreUsuario.setText(result.getNombre());
                    textViewApellidosUsuario.setText(result.getApellidos());
                    textViewCorreoUsuario.setText(result.getEmail());

                    Picasso.with(ctx)
                            .load(result.getPhoto())
                            .transform(new CircleTransform())
                            .resize(1300, 700)
                            .centerCrop()
                            .into(imageViewFotoUsuario);

                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_usuarioFragment), Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.fallo_conexion), Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }

    //Metodos para subir una imagen con multiparte

    public void pickFromGal(){
        Intent getIntent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        getIntent.setType("image/*");
        startActivityForResult(getIntent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            if(requestCode==PICK_IMAGE){
                onSelectFromGalleryResult(data);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
            try {
                Uri chosenImageUri = data.getData();
                selectedImage = chosenImageUri;
                Picasso
                        .with(ctx)
                        .load(chosenImageUri)
                        .transform(new CircleTransform())
                        .into(imageViewFotoUsuario);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean mayRequestStoragePermission(View view) {

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if((getActivity().checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (getActivity().checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED))
            return true;

        if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) || (shouldShowRequestPermissionRationale(CAMERA))){
            Snackbar.make(view, getString(R.string.permisos_usuarioFragment),
                    Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {

                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, 100);
                }
            });
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, 100);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 100){
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getActivity(), getString(R.string.permisosAceptados_usuarioFragment), Toast.LENGTH_SHORT).show();
                //mOptionButton.setEnabled(true);
            }
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getString(R.string.permisosDenegados_usuarioFragment));
            builder.setMessage(getString(R.string.permisos2_usuarioFragment));
            builder.setPositiveButton(getString(R.string.boton_dialog_aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.show();
        }
    }

    // AQUI TERMINAR LA IMPLEMENTACION DE IMAGEN


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IOnUsuarioInteractionListener) {
            mListener = (IOnUsuarioInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}

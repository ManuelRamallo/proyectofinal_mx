package com.mrdiaz.mx.models;

/**
 * Created by mrdiaz on 19/03/2018.
 */

public class Carreras {

    private String _id;
    private String nombre_circuito;
    private String nombre_lugar;
    private String direccion;
    private String fecha;
    private String web;
    private String telefono;
    private String pais;
    private String foto_carrera;
    private String url_entrada;

    public Carreras() {
    }

    public Carreras(String nombre_circuito, String nombre_lugar, String direccion, String fecha, String web, String telefono, String pais, String foto_carrera, String url_entrada) {
        this.nombre_circuito = nombre_circuito;
        this.nombre_lugar = nombre_lugar;
        this.direccion = direccion;
        this.fecha = fecha;
        this.web = web;
        this.telefono = telefono;
        this.pais = pais;
        this.foto_carrera = foto_carrera;
        this.url_entrada = url_entrada;
    }

    public String getNombre_circuito() {
        return nombre_circuito;
    }

    public void setNombre_circuito(String nombre_circuito) {
        this.nombre_circuito = nombre_circuito;
    }

    public String getUrl_entrada() {
        return url_entrada;
    }

    public void setUrl_entrada(String url_entrada) {
        this.url_entrada = url_entrada;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getNombre_lugar() {
        return nombre_lugar;
    }

    public void setNombre_lugar(String nombre_lugar) {
        this.nombre_lugar = nombre_lugar;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getFoto_carrera() {
        return foto_carrera;
    }

    public void setFoto_carrera(String foto_carrera) {
        this.foto_carrera = foto_carrera;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Carreras carreras = (Carreras) o;

        if (_id != null ? !_id.equals(carreras._id) : carreras._id != null) return false;
        if (nombre_circuito != null ? !nombre_circuito.equals(carreras.nombre_circuito) : carreras.nombre_circuito != null)
            return false;
        if (nombre_lugar != null ? !nombre_lugar.equals(carreras.nombre_lugar) : carreras.nombre_lugar != null)
            return false;
        if (direccion != null ? !direccion.equals(carreras.direccion) : carreras.direccion != null)
            return false;
        if (fecha != null ? !fecha.equals(carreras.fecha) : carreras.fecha != null) return false;
        if (web != null ? !web.equals(carreras.web) : carreras.web != null) return false;
        if (telefono != null ? !telefono.equals(carreras.telefono) : carreras.telefono != null)
            return false;
        if (pais != null ? !pais.equals(carreras.pais) : carreras.pais != null) return false;
        if (foto_carrera != null ? !foto_carrera.equals(carreras.foto_carrera) : carreras.foto_carrera != null)
            return false;
        return url_entrada != null ? url_entrada.equals(carreras.url_entrada) : carreras.url_entrada == null;
    }

    @Override
    public int hashCode() {
        int result = _id != null ? _id.hashCode() : 0;
        result = 31 * result + (nombre_circuito != null ? nombre_circuito.hashCode() : 0);
        result = 31 * result + (nombre_lugar != null ? nombre_lugar.hashCode() : 0);
        result = 31 * result + (direccion != null ? direccion.hashCode() : 0);
        result = 31 * result + (fecha != null ? fecha.hashCode() : 0);
        result = 31 * result + (web != null ? web.hashCode() : 0);
        result = 31 * result + (telefono != null ? telefono.hashCode() : 0);
        result = 31 * result + (pais != null ? pais.hashCode() : 0);
        result = 31 * result + (foto_carrera != null ? foto_carrera.hashCode() : 0);
        result = 31 * result + (url_entrada != null ? url_entrada.hashCode() : 0);
        return result;
    }
}

package com.mrdiaz.mx.models;

/**
 * Created by ridex on 19/03/2018.
 */

public class Pilotos {

    private String _id;
    private String nombre_completo;
    private String nombre;
    private String equipo;
    private String edad;
    private String nacionalidad;
    private String dorsal;
    private String titulos_mundiales;
    private String titulos_gp;
    private String primer_gp;
    private String foto_piloto_perfil;
    private String foto_piloto;

    public Pilotos() {
    }

    public Pilotos(String nombre_completo, String nombre, String equipo, String edad, String nacionalidad, String dorsal, String titulos_mundiales, String titulos_gp, String primer_gp, String foto_piloto_perfil, String foto_piloto) {
        this.nombre_completo = nombre_completo;
        this.nombre = nombre;
        this.equipo = equipo;
        this.edad = edad;
        this.nacionalidad = nacionalidad;
        this.dorsal = dorsal;
        this.titulos_mundiales = titulos_mundiales;
        this.titulos_gp = titulos_gp;
        this.primer_gp = primer_gp;
        this.foto_piloto_perfil = foto_piloto_perfil;
        this.foto_piloto = foto_piloto;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getNombre_completo() {
        return nombre_completo;
    }

    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getDorsal() {
        return dorsal;
    }

    public void setDorsal(String dorsal) {
        this.dorsal = dorsal;
    }

    public String getTitulos_mundiales() {
        return titulos_mundiales;
    }

    public void setTitulos_mundiales(String titulos_mundiales) {
        this.titulos_mundiales = titulos_mundiales;
    }

    public String getTitulos_gp() {
        return titulos_gp;
    }

    public void setTitulos_gp(String titulos_gp) {
        this.titulos_gp = titulos_gp;
    }

    public String getPrimer_gp() {
        return primer_gp;
    }

    public void setPrimer_gp(String primer_gp) {
        this.primer_gp = primer_gp;
    }

    public String getFoto_piloto_perfil() {
        return foto_piloto_perfil;
    }

    public void setFoto_piloto_perfil(String foto_piloto_perfil) {
        this.foto_piloto_perfil = foto_piloto_perfil;
    }

    public String getFoto_piloto() {
        return foto_piloto;
    }

    public void setFoto_piloto(String foto_piloto) {
        this.foto_piloto = foto_piloto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pilotos pilotos = (Pilotos) o;

        if (_id != null ? !_id.equals(pilotos._id) : pilotos._id != null) return false;
        if (nombre_completo != null ? !nombre_completo.equals(pilotos.nombre_completo) : pilotos.nombre_completo != null)
            return false;
        if (nombre != null ? !nombre.equals(pilotos.nombre) : pilotos.nombre != null) return false;
        if (equipo != null ? !equipo.equals(pilotos.equipo) : pilotos.equipo != null) return false;
        if (edad != null ? !edad.equals(pilotos.edad) : pilotos.edad != null) return false;
        if (nacionalidad != null ? !nacionalidad.equals(pilotos.nacionalidad) : pilotos.nacionalidad != null)
            return false;
        if (dorsal != null ? !dorsal.equals(pilotos.dorsal) : pilotos.dorsal != null) return false;
        if (titulos_mundiales != null ? !titulos_mundiales.equals(pilotos.titulos_mundiales) : pilotos.titulos_mundiales != null)
            return false;
        if (titulos_gp != null ? !titulos_gp.equals(pilotos.titulos_gp) : pilotos.titulos_gp != null)
            return false;
        if (primer_gp != null ? !primer_gp.equals(pilotos.primer_gp) : pilotos.primer_gp != null)
            return false;
        if (foto_piloto_perfil != null ? !foto_piloto_perfil.equals(pilotos.foto_piloto_perfil) : pilotos.foto_piloto_perfil != null)
            return false;
        return foto_piloto != null ? foto_piloto.equals(pilotos.foto_piloto) : pilotos.foto_piloto == null;
    }

    @Override
    public int hashCode() {
        int result = _id != null ? _id.hashCode() : 0;
        result = 31 * result + (nombre_completo != null ? nombre_completo.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (equipo != null ? equipo.hashCode() : 0);
        result = 31 * result + (edad != null ? edad.hashCode() : 0);
        result = 31 * result + (nacionalidad != null ? nacionalidad.hashCode() : 0);
        result = 31 * result + (dorsal != null ? dorsal.hashCode() : 0);
        result = 31 * result + (titulos_mundiales != null ? titulos_mundiales.hashCode() : 0);
        result = 31 * result + (titulos_gp != null ? titulos_gp.hashCode() : 0);
        result = 31 * result + (primer_gp != null ? primer_gp.hashCode() : 0);
        result = 31 * result + (foto_piloto_perfil != null ? foto_piloto_perfil.hashCode() : 0);
        result = 31 * result + (foto_piloto != null ? foto_piloto.hashCode() : 0);
        return result;
    }
}
